package com.example.diaryapp.ui.elements.day.blocks

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel
import com.example.diaryapp.ui.theme.bigPadding
import com.example.diaryapp.ui.theme.mediumPadding
import com.example.diaryapp.ui.theme.smallPadding

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MoodSelector(
    moods: List<Int>,
    viewModel: AddEditDayViewModel
) {
    val padding = 8.dp
    val density = LocalDensity.current
    var textDescription: String = "mood"
    val grayScaleMatrix = ColorMatrix(
        floatArrayOf(
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0f, 0f, 0f, 1f, 0f
        )
    )
    var showGray:Boolean = true
    Column(
        modifier = Modifier
            .padding(mediumPadding, bigPadding, mediumPadding, smallPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary) //color
            .fillMaxWidth()
    ){
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(mediumPadding, bigPadding)
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.primary)
        ) {
            Text(
                text = stringResource(id = R.string.day_status_mood_text),
                textAlign = TextAlign.Center,
                fontSize = 24.sp,
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically
        ) {

            moods.forEach { item ->
                Row{
                    if (viewModel.dayMood.value != item) {
                        textDescription = "selected mood"
                        showGray = true
                    } else {
                        textDescription = "unselected mood"
                        showGray = false
                    }
                    Surface(
                        shape = CircleShape,
                        elevation = 12.dp,
                        modifier = Modifier
                            .padding(padding)
                            .drawWithContent {
                                val paddingPx = with(density) { padding.toPx() }
                                clipRect(
                                    left = -paddingPx,
                                    top = 0f,
                                    right = size.width + paddingPx,
                                    bottom = size.height + paddingPx
                                ) {
                                    this@drawWithContent.drawContent()
                                }
                            },
                        onClick = {
                            viewModel.onEvent(AddEditDayEvent.SelectedMood(item))
                        }
                    ){
                        Image(
                            painterResource(id = item),
                            textDescription,
                            modifier = Modifier.padding(8.dp),
                            colorFilter = if (showGray) {
                                ColorFilter.colorMatrix(grayScaleMatrix)} else { null }
                        )

                    }
                }

            }

        }
    }
}