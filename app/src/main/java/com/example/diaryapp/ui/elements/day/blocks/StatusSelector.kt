package com.example.diaryapp.ui.elements.day.blocks

import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel
import com.example.diaryapp.ui.theme.bigPadding
import com.example.diaryapp.ui.theme.mediumPadding
import com.example.diaryapp.ui.theme.smallPadding
import com.google.accompanist.flowlayout.FlowRow
import com.google.accompanist.flowlayout.MainAxisAlignment
import com.google.accompanist.flowlayout.SizeMode

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun StatusSelector(
    icons: List<Int>,
    viewModel: AddEditDayViewModel
) {

    val _selected_icons = mutableListOf<Int>()
    _selected_icons.addAll(viewModel.dayWeather.value)
    var textDescription: String = "icon"
    val grayScaleMatrix = ColorMatrix(
        floatArrayOf(
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0.33f, 0.33f, 0.33f, 0f, 0f,
            0f, 0f, 0f, 1f, 0f
        )
    )
    var showGray:Boolean = true

    FlowRow(
        mainAxisAlignment = MainAxisAlignment.Center,
        mainAxisSize = SizeMode.Expand,
        crossAxisSpacing = 4.dp,
        mainAxisSpacing = 4.dp,
        modifier = Modifier
            .padding(mediumPadding, bigPadding, mediumPadding, smallPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary) //color
            .fillMaxWidth()

    ){
        icons.forEach{icon: Int ->
            if (icon !in _selected_icons) {
                textDescription = "selected icon"
                showGray = true
            } else {
                textDescription = "unselected icon"
                showGray = false
            }
            Surface(
                shape = CircleShape,
                elevation = smallPadding,
                modifier = Modifier
                    .padding(mediumPadding)
                    .drawWithContent {
                        val paddingPx = with(density) { mediumPadding.toPx() }
                        clipRect(
                            left = -paddingPx,
                            top = 0f,
                            right = size.width + paddingPx,
                            bottom = size.height + paddingPx
                        ) {
                            this@drawWithContent.drawContent()
                        }
                    },
                onClick = {
                    if (icon in _selected_icons) {
                        _selected_icons.remove(icon)
                    } else {
                        _selected_icons.add(icon)
                    }
                    Log.d("event", "${_selected_icons.size}:${_selected_icons.joinToString()}")
                    viewModel.onEvent(AddEditDayEvent.SelectedStatus(_selected_icons))

                }
            ) {
                Image(
                    painter = painterResource(
                        id = icon),
                    contentDescription = "image icon",
                    modifier = Modifier.padding(mediumPadding),
                    colorFilter = if (showGray) {
                        ColorFilter.colorMatrix(grayScaleMatrix)} else { null }

                )
            }

        }
    }

}