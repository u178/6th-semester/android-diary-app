package com.example.diaryapp.ui.elements.calendar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.unit.dp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.elements.calendar.YearSpinner
import com.example.diaryapp.ui.elements.calendar.MonthSpinner
import com.example.diaryapp.ui.theme.*

@Composable
fun MonthAndYearSelector(
    viewModel: SelectDateViewModel
) {
    val months  = stringArrayResource(R.array.months)
    val years  = stringArrayResource(R.array.years)
    Row(
        modifier = Modifier
            .padding(mediumPadding, bigPadding, mediumPadding, smallPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.End,
        verticalAlignment = Alignment.CenterVertically
    ) {

        Row() {
            MonthSpinner(
                viewModel = viewModel,
                items = months
            )

            YearSpinner(
                viewModel = viewModel,
                items = years
            )
            Spacer(
                modifier = Modifier
                    .padding(0.dp, 0.dp, 10.dp, 0.dp)
            )
        }
    }

}