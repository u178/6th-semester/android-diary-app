package com.example.diaryapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val WhiteSmoke = Color(0xFFF5F5F5)
val Gainsboro = Color(0xFFDCDCDC)
val lgray = Color(0xFFE8E8E8)

val NavigationBarItem = Color(0xFFCC34FF)
val PurpleMain = Color(0xFFD9C9EE)
val PurpleAccent = Color(0xFFB494D7)
val Mint = Color(0xFFB2E5DC)
val DarkMint = Color(0xFF2B8EA1)
val Navy = Color(0xFF0E5492)