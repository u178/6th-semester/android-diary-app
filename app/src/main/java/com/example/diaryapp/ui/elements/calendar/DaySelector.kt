package com.example.diaryapp.ui.elements.calendar

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.theme.*

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DaySelector(
    viewModel: SelectDateViewModel,
    navController: NavController,
    emptyDays: Int,
    days: Int
) {
    Column() {
        MonthAndYearSelector(viewModel = viewModel)
        DayBar(viewModel = viewModel)

        DaysGrid(
            viewModel = viewModel,
            navController = navController,
            emptyDays = emptyDays,
            days = days
        )
    }

}





