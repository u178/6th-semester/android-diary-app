package com.example.diaryapp.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(4.dp),
    large = RoundedCornerShape(0.dp)
)

val zeroPadding = 0.dp
val tinyPadding = 2.dp
val smallPadding = 4.dp
val mediumPadding = 6.dp
val bigPadding = 8.dp
val giantPadding = 12.dp