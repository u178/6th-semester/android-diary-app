package com.example.diaryapp.ui.theme

import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.BottomNavigation
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.compose.material.icons.outlined.DateRange
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.example.diaryapp.Screen
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun BottomNavBar(currentScreen: MutableState<Screen>, navHostController: NavHostController) {

    CustomNavigationBar(currentScreenId = currentScreen.value.route, navHostController = navHostController) {
        currentScreen.value = it
    }
}


@Composable
fun CustomNavigationBar(
    currentScreenId: String,
    navHostController: NavHostController,
    onItemSelected: (Screen) -> Unit
) {
    val items = Screen.Items.list
    val navBackStackEntry by navHostController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route

    Row(
        modifier = Modifier
            .padding(0.dp, 10.dp, 0.dp, 0.dp)
//            .clip(RoundedCornerShape(10.dp))
            .background(colors.primary) //color
            .padding(0.dp, 6.dp, 0.dp, 6.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.CenterVertically
    ) {

        items.forEach { item ->

            var curRoute = currentRoute
            if (curRoute != null && curRoute > item.route) {
                val i = curRoute.indexOf("?")
                if (i > 0) {
                    curRoute = curRoute.substring(0, i)
                }
            }
            CustomBottomNavItem(item = item, isSelected = item.route == curRoute) {
//                onItemSelected(item)
                var cur_route = item.route
                if (item.route == Screen.Day.route) {
                     cur_route = item.route + "?dayId=${SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)
                         .format(Date()).toString().toInt()}"
                }
                Log.d("event", "cur route: $cur_route")
                navHostController.navigate(cur_route) {
                    launchSingleTop = true
                    popUpTo(navHostController.graph.findStartDestination().id) {
                        saveState = true
                    }
                    restoreState = true
                }
            }
        }
    }
}


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun CustomBottomNavItem(item: Screen, isSelected: Boolean, onClick: () -> Unit) {

    val boxModifier: Modifier = if (isSelected) {
        Modifier
            .clip(RoundedCornerShape(20.dp))
            .background(colors.primaryVariant)
            .clickable(onClick = onClick)
//            .border(5.dp, Color.White)
//            .shadow(0.dp, RoundedCornerShape(2.dp), false)

    } else {
        Modifier
            .clip(CircleShape)
            .clickable(onClick = onClick)
    }

    Box(
        modifier = boxModifier,
    ) {
        Row(
//            Modifier.shadow()
            modifier = Modifier
                .padding(8.dp),

            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(4.dp)
        ) {

            Icon(
                imageVector = item.icon,
                contentDescription = null,
                tint = Color.White // COLOR
            )

            AnimatedVisibility(visible = isSelected) {
                Text(
                    text = stringResource(id = item.title),
                    color = Color.Black //COLOR
                )
            }

        }
    }
}
