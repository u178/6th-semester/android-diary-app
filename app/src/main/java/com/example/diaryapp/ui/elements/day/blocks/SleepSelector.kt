package com.example.diaryapp.ui.elements.day.blocks

import android.content.res.Resources
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel


@Composable
fun SleepSelector(viewModel: AddEditDayViewModel) {
    val density = LocalDensity.current
    var text: String
    var time: Int
    var sliderPosition by remember { mutableStateOf(0f) }
    sliderPosition = viewModel.daySleep.value.toFloat()
    Column(
        modifier = Modifier
            .padding(6.dp, 8.dp)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary) //color
            .fillMaxWidth(),
    ){
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(6.dp, 8.dp, 6.dp, 3.dp)
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.primary)
        ) {
            Text(
                text = stringResource(id = R.string.day_status_sleep_text),
                textAlign = TextAlign.Center,
                fontSize = 24.sp,
            )
        }
//        Text(text = "Текущее значение: ${sliderPosition}", fontSize = 22.sp)
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(6.dp, 3.dp, 6.dp, 8.dp)
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.primary)
        ) {
            Text(
                textAlign = TextAlign.Center,
                text = "${maxOf(sliderPosition.toInt() / 6, 0)}:${maxOf(sliderPosition.toInt() % 6, 0)}0",
                fontSize = 28.sp,
                fontWeight = FontWeight.Bold
            )
        }


        Slider(
            value = sliderPosition,
            onValueChange = {
                sliderPosition = it
                viewModel.onEvent(AddEditDayEvent.EnteredSleep(it.toInt()))
                            },
            valueRange = 0f..143f,
//            steps = 144,
            modifier = Modifier
                .border(0.dp, Color.White)
                .padding(6.dp, 8.dp),
            colors = SliderDefaults.colors(
                activeTrackColor = Color.White,
                inactiveTrackColor = Color.White,
                thumbColor = MaterialTheme.colors.onPrimary
            )
        )
    }
}

@Composable
fun SliderLabel(
    label: String,
    minWidth: Dp,
    modifier: Modifier = Modifier
) {
    Text(
        text = label,
        textAlign = TextAlign.Center,
        color = Color.White, // COLOR
        modifier = modifier
            .background(
                color = MaterialTheme.colors.primary, // COLOR
                shape = RoundedCornerShape(4.dp)
            )
            .padding(4.dp)
            .defaultMinSize(minWidth = minWidth)
    )
}

private fun getSliderOffset(
    value: Float,
    valueRange: ClosedFloatingPointRange<Float>,
    boxWidth: Dp,
    labelWidth: Dp
): Dp {
    val coerced = value.coerceIn(valueRange.start, valueRange.endInclusive)
    val positionFraction = calcFraction(valueRange.start, valueRange.endInclusive, coerced)

    return (boxWidth - labelWidth) * positionFraction
}

private fun calcFraction(a: Float, b: Float, pos: Float) =
    (if (b - a == 0f) 0f else (pos - a) / (b - a)).coerceIn(0f,1f)




