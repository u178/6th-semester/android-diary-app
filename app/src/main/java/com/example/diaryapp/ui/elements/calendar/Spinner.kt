package com.example.diaryapp.ui.elements.calendar

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.diaryapp.day_feature.presentation.select_date.DateEvent
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.theme.mediumPadding
import com.example.diaryapp.ui.theme.smallPadding


@Composable
fun YearSpinner(
    viewModel: SelectDateViewModel,
    items: Array<String>
) {
    var expanded by remember { mutableStateOf(false) }
    var selectedIndex by remember { mutableStateOf(viewModel.year.value) }
    Surface(
        modifier = Modifier
            .wrapContentSize()
            .padding(smallPadding, mediumPadding)
            .clip(RoundedCornerShape(20.dp))
            .clickable(onClick = { expanded = true })
    ) {
        Text(
            text = viewModel.year.value.toString(),
            modifier = Modifier
                .wrapContentSize()
                .background(Color.White)
                .padding(smallPadding)

        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .wrapContentSize()
                .background(
                    Color.White
                )
        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(onClick = {
                    viewModel.onEvent(DateEvent.SelectYear(s.toInt()))
                    Log.d("event", "changed Year. Cur year: ${viewModel.year.value}")

                    expanded = false
                }) {
                    Text(text = s,
                        fontSize = 24.sp,
                        textAlign = TextAlign.Center)
                }
            }
        }
    }
}

@Composable
fun MonthSpinner(
    viewModel: SelectDateViewModel,
    items: Array<String>
) {
    var expanded by remember { mutableStateOf(false) }
    val selectedIndex by remember { mutableStateOf(viewModel.month.value) }
    Surface(
        modifier = Modifier
            .wrapContentSize()
            .padding(smallPadding, mediumPadding)
            .clip(RoundedCornerShape(20.dp))
            .clickable(onClick = { expanded = true })

    ) {
        Text(
//            items[selectedIndex],
            items[viewModel.month.value - 1],
            modifier = Modifier
                .wrapContentSize()
                .background(Color.White)
                .padding(smallPadding)
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .wrapContentSize()
                .background(
                    Color.White
                )
        ) {
            items.forEachIndexed { index, s ->
                DropdownMenuItem(onClick = {
                    expanded = false
                    viewModel.onEvent(DateEvent.SelectMonth(index + 1))
                    Log.d("event", "changed month. Cur month: ${viewModel.month.value}")

                }) {
                    Text(text = s,
                        fontSize = 24.sp,
                        textAlign = TextAlign.Center,
                    )

                }
            }
        }
    }
}