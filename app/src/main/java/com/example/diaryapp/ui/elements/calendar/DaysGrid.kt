package com.example.diaryapp.ui.elements.calendar

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import com.example.diaryapp.Screen
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.theme.*
import java.text.SimpleDateFormat
import java.util.*

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun DaysGrid(
    viewModel: SelectDateViewModel,
    navController: NavController,
    emptyDays: Int,
    days: Int
) {
    val curDate = SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)
        .format(Date()).toString().toInt()
    val context = LocalContext.current

    LazyVerticalGrid(
        cells = GridCells.Fixed(7),
        content = {
            items(viewModel.emptyDays.value) {i->

                Surface(
//                    shape = CircleShape,
//                    elevation = bigPadding,
                    modifier = Modifier
                        .padding(mediumPadding, bigPadding)
                        .aspectRatio(1f)
                        .clip(RoundedCornerShape(20.dp))
                        .background(Color.White),
                    color = MaterialTheme.colors.primary,
                    elevation = 8.dp
                    ) {
                    Text(
                        text= " ",
                        textAlign = TextAlign.Center,
                        fontSize = 24.sp,
                        modifier = Modifier
                            .padding(mediumPadding)

                    )

                }

            };
            items(viewModel.daysInMonth.value) {i->
                val m = viewModel.month.value
                val dayId = (
                        viewModel.year.value.toString() +
                                if (m < 9) {'0' + m.toString()} else {m.toString()}   +
                                if (i < 9) {"0${i+1}"} else {(i+1).toString()})
                    .toInt()
                    Surface(
                        modifier = Modifier
                            .padding(mediumPadding, bigPadding)
                            .aspectRatio(1f)
                            .clip(RoundedCornerShape(20.dp))
                            .background(Color.White),
                        onClick = {
                            if (dayId <= curDate) {
                                Log.d("event", "cur date $curDate next date $dayId")
                                navController.navigate(Screen.Day.route + "?dayId=${dayId}") {
                                    launchSingleTop = true
                                    popUpTo(navController.graph.findStartDestination().id) {
                                        saveState = true
                                    }
                                    restoreState = true
                                }
                            }
                        },
                        color = if (dayId <= curDate) {Color.White} else {
                            lgray}
                    ) {
                        Text(
                            text = "${i + 1}",
                            textAlign = TextAlign.Center,
                            fontSize = 24.sp,
                            modifier = Modifier
                                .padding(mediumPadding)
                        )
                    }
            };


        },
        modifier = Modifier
            .padding(mediumPadding, smallPadding, mediumPadding, smallPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary) //color
            .fillMaxWidth()
    )
}