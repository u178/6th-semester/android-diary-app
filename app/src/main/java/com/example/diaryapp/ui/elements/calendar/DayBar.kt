package com.example.diaryapp.ui.elements.calendar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.unit.dp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.theme.*

@Composable
fun DayBar(
    viewModel: SelectDateViewModel
) {
    val days  = stringArrayResource(R.array.days)

    Row(
        modifier = Modifier
            .padding(mediumPadding, mediumPadding, mediumPadding, smallPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.CenterVertically
    ) {
        days.forEach { day ->

            Surface(
                shape = CircleShape,
                modifier = Modifier
//                        .aspectRatio(1f)
                    .padding(smallPadding)
                    .clip(RoundedCornerShape(22.dp))
                    .background(Color.White),
            ) {
                Text(
                    text = day,
                    modifier = Modifier
                        .padding(smallPadding))
            }
        }

    }

}
