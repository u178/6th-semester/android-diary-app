package com.example.diaryapp.ui.elements.day.blocks

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel
import com.example.diaryapp.ui.theme.*

@Composable
fun EditTextField(
    viewModel: AddEditDayViewModel
) {
    Column(
        modifier = Modifier
            .padding(mediumPadding, bigPadding, mediumPadding, bigPadding)
            .clip(RoundedCornerShape(10.dp))
            .background(MaterialTheme.colors.primary) //color
            .fillMaxWidth()
    ) {
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(mediumPadding, bigPadding, mediumPadding, smallPadding)
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.primary)
        ) {
            Text(
                text = stringResource(id = R.string.day_status_text_text),
                textAlign = TextAlign.Center,
                fontSize = 24.sp,
            )
        }

        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .padding(mediumPadding, smallPadding, mediumPadding, mediumPadding)
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.primary)
        ) {
            BasicTextField(
                value = viewModel.dayText.value,
                onValueChange = {
                    viewModel.onEvent(AddEditDayEvent.EnteredText(it))
                                },
                singleLine = false,
                textStyle = Typography.body1,
                maxLines = 20,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(mediumPadding, bigPadding)
            )
        }
    }
}