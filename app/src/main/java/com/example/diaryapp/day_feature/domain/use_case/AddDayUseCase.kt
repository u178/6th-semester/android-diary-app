package com.example.diaryapp.day_feature.domain.use_case

import android.util.Log
import com.example.diaryapp.day_feature.domain.models.Day
import com.example.diaryapp.day_feature.domain.repository.DayRepository

class AddDayUseCase(
    private val repository: DayRepository
) {
    suspend operator fun invoke(day: Day) {
        // additional logic
        Log.d("event", day.toString())
        repository.insertDay(day)
    }
}