package com.example.diaryapp.day_feature.presentation.days

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.diaryapp.day_feature.domain.use_case.DayUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DaysViewModel @Inject constructor(
    private val dayUseCases: DayUseCases
) : ViewModel() {

    private val _state = mutableStateOf(DaysState())
    val state: State<DaysState> = _state

    fun onEvent(event: DaysEvent) {
        when(event) {
            is DaysEvent.Mood -> {

            }
            is DaysEvent.Sleep -> {
//                _state.value = state.value.copy()

            }
            is DaysEvent.Weather -> {

            }


        }
    }

}