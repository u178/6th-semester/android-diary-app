package com.example.diaryapp.day_feature.domain.use_case

import com.example.diaryapp.day_feature.domain.models.Day
import com.example.diaryapp.day_feature.domain.repository.DayRepository

class DeleteDayUseCase(
    private val repository: DayRepository
) {
    suspend operator fun invoke(day: Day) {
        repository.deleteDay(day)
    }
}