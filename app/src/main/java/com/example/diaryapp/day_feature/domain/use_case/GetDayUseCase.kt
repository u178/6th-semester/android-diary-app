package com.example.diaryapp.day_feature.domain.use_case

import com.example.diaryapp.day_feature.domain.models.Day
import com.example.diaryapp.day_feature.domain.repository.DayRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetDayUseCase (
    private val repository: DayRepository
        ){

    suspend operator fun invoke(id: Int): Day? {
        return repository.getDayById(id)
    }
}