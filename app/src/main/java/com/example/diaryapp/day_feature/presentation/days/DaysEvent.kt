package com.example.diaryapp.day_feature.presentation.days

sealed class DaysEvent {
    data class Mood(val icon_id: Int): DaysEvent()
    data class Sleep(val time_sleep: Int): DaysEvent()
    data class Weather(val weather_list: List<Int>): DaysEvent()
//    data class (val weather_list: List<Int>): DaysEvent()
}
