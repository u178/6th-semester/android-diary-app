package com.example.diaryapp.day_feature.presentation.add_edit_day

import androidx.compose.ui.focus.FocusState

sealed class AddEditDayEvent{

    data class SelectedMood(val value: Int): AddEditDayEvent()
    data class EnteredSleep(val value: Int): AddEditDayEvent()
    data class SelectedWeather(val value: List<Int>): AddEditDayEvent()
    data class EnteredText(val value: String): AddEditDayEvent()
    data class SelectedStatus(val value: List<Int>): AddEditDayEvent()
    data class ChangeTextFocus(val focusState: FocusState): AddEditDayEvent()
    object SaveDay: AddEditDayEvent()
}
