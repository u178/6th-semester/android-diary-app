package com.example.diaryapp.day_feature.domain.use_case

import android.util.Log

data class DayUseCases(
    val getDayUseCase: GetDayUseCase,
    val deleteDayUseCase: DeleteDayUseCase,
    val addDayUseCase: AddDayUseCase,
)
//ToDo add getDayByDateUseCase
//ToDo add getDaysByMonthsUseCase
