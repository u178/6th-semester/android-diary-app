package com.example.diaryapp.day_feature.presentation.select_date

sealed class DateEvent {
    data class SelectMonth(val value: Int): DateEvent()
    data class SelectYear(val value: Int): DateEvent()
}