package com.example.diaryapp.day_feature.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import java.util.*

@Entity
data class Day(
    val mood: Int = -1,
    val sleep: Int = -1,
    val weather: String = "",
    val date: String = "",
    val text: String = "",
    val json: String = "",
    @PrimaryKey val id: Int? = null
)