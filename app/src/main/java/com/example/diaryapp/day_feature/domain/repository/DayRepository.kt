package com.example.diaryapp.day_feature.domain.repository

import com.example.diaryapp.day_feature.domain.models.Day
import kotlinx.coroutines.flow.Flow


interface DayRepository {

    fun getDays() : Flow<List<Day>>

    suspend fun getDayById(id: Int) : Day?

//    suspend fun getDayByDate(date: String): Day?

    suspend fun insertDay(day: Day)

    suspend fun deleteDay(day: Day)

}