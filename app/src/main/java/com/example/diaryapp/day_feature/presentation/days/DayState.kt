package com.example.diaryapp.day_feature.presentation.days

import com.example.diaryapp.day_feature.domain.models.Day

data class DaysState(
    val days: List<Day> = emptyList(),
    val mood: Int = -1,
    val sleep: Int = 0,
    val weather: List<Int> = emptyList()


)
