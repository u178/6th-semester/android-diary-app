package com.example.diaryapp.day_feature.presentation.add_edit_day

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.diaryapp.day_feature.domain.models.Day
import com.example.diaryapp.day_feature.domain.use_case.DayUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class AddEditDayViewModel @Inject constructor(
    private val dayUseCases: DayUseCases,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _dayMood = mutableStateOf(-1)
    val dayMood: State<Int> = _dayMood

    private val _daySleep = mutableStateOf(-1)
    val daySleep: State<Int> = _daySleep

    private val _dayWeather = mutableStateOf(listOf<Int>())
    val dayWeather: State<List<Int>> = _dayWeather

    private val _dayStatus = mutableStateOf(listOf<Int>())
    val dayStatus: State<List<Int>> = _dayStatus

    private val _dayText = mutableStateOf(String())
    val dayText: State<String> = _dayText

    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val dateFormatter = SimpleDateFormat("yyyyMMdd", Locale.ENGLISH)
    private var currentDayId: Int? = dateFormatter.format(Date()).toString().toInt()
    private var currentDayDate: String? = null

    init {
        savedStateHandle.get<Int>("dayId")?.let { dayId ->
            if (dayId != -1) {
                viewModelScope.launch {
                    dayUseCases.getDayUseCase(dayId)?.also { day ->
                        Log.d("event", "AddEditDay init" + day.toString())
                        currentDayDate = day.date
                        _dayMood.value = day.mood
                        _daySleep.value = day.sleep
                        // it stores as String but we use ListofInt
                        if (day.weather.isBlank()) {
                            _dayWeather.value = listOf()
                        } else {
                            _dayWeather.value = day.weather.replace(" ", "")
                                .split(',').map { it.toInt() }.toList()
                        }

                        if (day.json.isBlank()) {
                            _dayStatus.value = listOf()
                        } else {
                            _dayStatus.value = day.json.replace(" ", "")
                                .split(',').map { it.toInt() }.toList()
                        }

                        _dayText.value = day.text
                        Log.d("event", "init text: $dayText")
                    }
                    currentDayId = dayId
                }
                Log.d("event", "Init, day id: ${dayId}")
            }
        }
    }
    fun onEvent(event: AddEditDayEvent) {
        when (event) {
            is AddEditDayEvent.SelectedMood -> {
                _dayMood.value = event.value
            }

            is AddEditDayEvent.EnteredSleep -> {
                _daySleep.value = event.value
            }

            is AddEditDayEvent.SelectedWeather -> {
                _dayWeather.value = event.value
            }

            is AddEditDayEvent.SelectedStatus -> {
                _dayWeather.value = event.value
            }

            is AddEditDayEvent.EnteredText -> {
                _dayText.value = event.value
            }

            is AddEditDayEvent.SaveDay -> {
                viewModelScope.launch {
                    dayUseCases.addDayUseCase(
                        Day(
                            mood = dayMood.value,
                            sleep = daySleep.value,
                            weather = dayWeather.value.joinToString(", "),
                            text = dayText.value,
                            json = dayStatus.value.joinToString { ", " },
                            date = System.currentTimeMillis().toString(),
                            id = currentDayId
                        )
                    )
                    Log.d("event", "ViewModel event saveDay")
                    _eventFlow.emit(UiEvent.SaveDay)
                }
            }

        }
    }


    sealed class UiEvent {
        object SaveDay: UiEvent()
    }
}