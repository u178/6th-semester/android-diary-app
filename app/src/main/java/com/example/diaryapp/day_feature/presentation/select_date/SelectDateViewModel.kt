package com.example.diaryapp.day_feature.presentation.select_date

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.diaryapp.day_feature.domain.use_case.DayUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.YearMonth
import java.util.*
import javax.inject.Inject

@HiltViewModel
class SelectDateViewModel @Inject constructor(
    private val dayUseCases: DayUseCases,
    savedStateHandle: SavedStateHandle
): ViewModel() {

    private val dateFormatter = SimpleDateFormat("yyyyMM", Locale.ENGLISH)
    private var currentDate: String = dateFormatter.format(Date()).toString()

    private val _year = mutableStateOf(currentDate.substring(0, 4).toInt())
    val year: State<Int> = _year

    private val _month = mutableStateOf(currentDate.substring(5).toInt())
    val month: State<Int> = _month

    private val _emptyDays = mutableStateOf(LocalDate.of(_year.value, _month.value, 1)
        .dayOfWeek.value - 1)
    val emptyDays: State<Int> = _emptyDays

    private val _daysInMonth = mutableStateOf(YearMonth.of(_year.value, _month.value).lengthOfMonth())
    val daysInMonth: State<Int> = _daysInMonth

    init {

    }
    fun onEvent(event: DateEvent) {
        when (event) {
            is DateEvent.SelectYear -> {
                _year.value = event.value
                updateAmountOfDays()
            }

            is DateEvent.SelectMonth -> {
                _month.value = event.value
                updateAmountOfDays()
            }
        }
    }

    private fun updateAmountOfDays(){
        _emptyDays.value = LocalDate.of(_year.value, _month.value, 1)
            .dayOfWeek.value - 1
        _daysInMonth.value = YearMonth.of(_year.value, _month.value).lengthOfMonth()
        Log.d("event", "updateAmoutOfDay. empty days: ${_emptyDays.value}, " +
                "full days: ${_daysInMonth.value}")
    }
}

