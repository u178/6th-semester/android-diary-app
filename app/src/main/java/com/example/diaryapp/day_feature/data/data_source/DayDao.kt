package com.example.diaryapp.day_feature.data.data_source

import androidx.room.*
import com.example.diaryapp.day_feature.domain.models.Day
import kotlinx.coroutines.flow.Flow

@Dao
interface DayDao {

    @Query("SELECT * FROM day")
    fun getDays(): Flow<List<Day>>

    @Query("SELECT * FROM day WHERE id = :id")
    suspend fun  getDayById(id: Int): Day?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDay(day: Day)

    @Delete
    suspend fun deleteDay(day: Day)
}