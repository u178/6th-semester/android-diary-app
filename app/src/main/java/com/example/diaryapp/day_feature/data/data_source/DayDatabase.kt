package com.example.diaryapp.day_feature.data.data_source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.diaryapp.day_feature.domain.models.Day

@Database(
    entities = [Day::class],
    version = 1
)
abstract class DayDatabase: RoomDatabase() {

    abstract val dayDao: DayDao

    companion object {
        const val DATABASE_NAME = "days_db"
    }
}