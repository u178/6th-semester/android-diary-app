package com.example.diaryapp.day_feature.data.repository

import com.example.diaryapp.day_feature.data.data_source.DayDao
import com.example.diaryapp.day_feature.domain.models.Day
import com.example.diaryapp.day_feature.domain.repository.DayRepository
import kotlinx.coroutines.flow.Flow

class DayRepositoryImplementation(
    private val dao: DayDao
) : DayRepository {

    override fun getDays(): Flow<List<Day>> {
        return dao.getDays()
    }

    override suspend fun getDayById(id: Int): Day? {
        return dao.getDayById(id)
    }

//    override suspend fun getDayByDate(date: String): Day? {
//        return dao.getDayByDate(date)
//    }

    override suspend fun insertDay(day: Day) {
        dao.insertDay(day)
    }

    override suspend fun deleteDay(day: Day) {
        dao.deleteDay(day)
    }
}