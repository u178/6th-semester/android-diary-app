package com.example.diaryapp

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.DateRange
import androidx.compose.material.icons.outlined.Face
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.ui.graphics.vector.ImageVector

sealed class Screen(val route: String, @StringRes val title: Int, val icon: ImageVector
) {
    object Calendar : Screen("Calendar", R.string.CALENDAR_SCREEN_TITLE, Icons.Outlined.DateRange)
    object Day : Screen("Day", R.string.DAY_SCREEN_TITLE, Icons.Outlined.Face)
    object Stats : Screen("Stats", R.string.STATS_SCREEN_TITLE, Icons.Outlined.Info)
    object Settings : Screen("Settings", R.string.SETTINGS_SCREEN_TITLE,Icons.Outlined.Settings)

    object Items {
        val list = listOf(
            Calendar, Day, Stats, Settings
        )
    }

//    fun withArgs(vararg args: String): String {
//        return buildString {
//            append(route)
//            args.forEach { arg ->
//                append("/$arg")
//            }
//        }
//    }


}
