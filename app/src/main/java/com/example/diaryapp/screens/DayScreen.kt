package com.example.diaryapp.screens

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.diaryapp.R
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel
import com.example.diaryapp.ui.elements.day.blocks.*
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayEvent
import com.example.diaryapp.ui.theme.mediumPadding
import com.example.diaryapp.ui.theme.smallPadding
import kotlinx.coroutines.flow.collectLatest

@Composable
fun DayScreen(
    navController: NavController,
    moods: List<Int>,
    weather: List<Int>,
    status: List<Int>,
    viewModel: AddEditDayViewModel = hiltViewModel()
) {
    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when(event) {
                is AddEditDayViewModel.UiEvent.SaveDay -> {
                    Log.d("event", "Day screen Event saveDay")
                    navController.navigateUp()
                }
            }
        }
    }
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ){
        item {

            Surface(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(6.dp, 8.dp)
            ) {
                Text(
                    textAlign = TextAlign.Center,
                    text = stringResource(id = R.string.day_screen_top_text),
                    fontSize = 32.sp,
                    fontWeight = FontWeight.Bold
                )
            }
        }


        // mood selector
        item {
            MoodSelector(
                moods = moods,
                viewModel = viewModel
            )
        }


        // Sleep selector
        item {
            SleepSelector(viewModel)
        }

        // Weather selector
        item {
            MultipleSelector(
                icons = weather,
                viewModel = viewModel
            )
        }
        
        item {
            EditTextField(viewModel = viewModel)
        }

        item{
            StatusSelector(
                viewModel = viewModel,
                icons = status
            )
        }

//        saveButton()
        item {
            Button(
                onClick = {
                    viewModel.onEvent(AddEditDayEvent.SaveDay)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(mediumPadding, smallPadding, mediumPadding, smallPadding)
                    .clip(RoundedCornerShape(10.dp))
                    .background(MaterialTheme.colors.primary)

            ) {
                Text(
                    "Запомнить день", //ToDo add resource
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    }
}






