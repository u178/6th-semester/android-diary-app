package com.example.diaryapp.screens

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.diaryapp.day_feature.presentation.add_edit_day.AddEditDayViewModel
import com.example.diaryapp.day_feature.presentation.select_date.SelectDateViewModel
import com.example.diaryapp.ui.elements.calendar.DaySelector




@Composable
fun CalendarScreen(
    navController: NavController,
    emptyDays: Int,
    days: Int,
    viewModel: SelectDateViewModel = hiltViewModel()
) {
    DaySelector(
        viewModel = viewModel,
        navController = navController,
        emptyDays = emptyDays,
        days = days
    )
}
