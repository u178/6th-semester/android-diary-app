package com.example.diaryapp.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.diaryapp.R
import com.example.diaryapp.Screen
import com.example.diaryapp.ui.theme.BottomNavBar

private val TAG = "ActivityScreen"


@SuppressLint("UnusedMaterialScaffoldPaddingParameter", "UnrememberedMutableState")
@Composable
fun ActivityScreen() {

    val navController = rememberNavController()

    val currentScreen = mutableStateOf<Screen>(Screen.Calendar)

    Scaffold(
        topBar = {
            TopAppBar(
                navController = navController
            )
        },
        bottomBar = {
            BottomNavBar(currentScreen = currentScreen, navHostController = navController)
        }
    ) { innerPadding ->
        Box(
            modifier = Modifier.padding(innerPadding)
        ) {
            NavHost(startDestination = Screen.Calendar.route, navController = navController) {
                composable(Screen.Calendar.route) {
                    CalendarScreen(
                        navController = navController,
                        emptyDays = 2,
                        days = 30
                    )
                }
                composable(
                    route = Screen.Day.route + "?dayId={dayId}",
                    arguments = listOf(
                        navArgument(
                            name = "dayId",
                        ) {
                            type = NavType.IntType
                            defaultValue = -1
                        }
                    )
                ) {
                    DayScreen(
                        navController = navController,
                        moods = listOf(
                            R.drawable.happiness,
                            R.drawable.indifferent,
                            R.drawable.sad
                        ),
                        weather = listOf(
                            R.drawable.day_clear,
                            R.drawable.rain,
                            R.drawable.snow,
                            R.drawable.thunder,
                            R.drawable.wind
                        ),
                        status = listOf(
                            R.drawable.workout, R.drawable.bucket, R.drawable.sick, R.drawable.student,
                            R.drawable.family, R.drawable.cinema, R.drawable.picnic, R.drawable.work
                        )
                    )
                }
                composable(Screen.Stats.route) { StatsScreen() }
                composable(Screen.Settings.route) { SettingsScreen() }
            }
        }
    }
}

@Composable
fun TopAppBar(navController: NavHostController) {

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route

    Log.d(TAG, "Current Route $currentRoute")


}
