package com.example.diaryapp

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material.icons.rounded.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.diaryapp.screens.ActivityScreen
import com.example.diaryapp.ui.theme.DiaryAppTheme
import dagger.hilt.android.AndroidEntryPoint

//import com.example.diaryapp.screens.Calendar
//import com.example.diaryapp.ui.theme.DiaryAppTheme
//import com.example.diaryapp.ui.theme.MyBottomNavigationBar
//
//import com.example.diaryapp.screens.Main
//import com.example.diaryapp.screens.Profile
//import com.example.diaryapp.screens.Welcome

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DiaryAppTheme {
                ActivityScreen()
            }
        }
    }
}


