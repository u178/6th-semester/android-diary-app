package com.example.diaryapp.di

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.diaryapp.day_feature.data.data_source.DayDatabase
import com.example.diaryapp.day_feature.data.repository.DayRepositoryImplementation
import com.example.diaryapp.day_feature.domain.repository.DayRepository
import com.example.diaryapp.day_feature.domain.use_case.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDayDatabase(app: Application): DayDatabase {
        return Room.databaseBuilder (
            app,
            DayDatabase::class.java,
            DayDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideDayRepository(db: DayDatabase): DayRepository {
        return DayRepositoryImplementation(db.dayDao)
    }

    @Provides
    @Singleton
    fun provideDayUseCases(repository: DayRepository): DayUseCases {
        return DayUseCases(
            getDayUseCase = GetDayUseCase(repository),
            deleteDayUseCase = DeleteDayUseCase(repository),
            addDayUseCase = AddDayUseCase(repository)
        //TODO add more use cases
        )
    }
}