# Diary application

*IT Academy Samsung & Practice project*



### Used techologies:

* Clean Architecture

* MVVM

* Single Activity

* Jetpack Compose

* DB - SQLite

* DI - Dagger Hilt

## Screens

**Calendar**

<img src="./.images/calendar.jpg" height="400">

**Empty day**

<img src="./.images/day_empty.jpg" height="400">

**Filled day**


<img src="./.images/day_filled.jpg" height="400">
